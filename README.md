# Daily Exercise
> 自律给我自由

## 2019

<table>
    <tr>
        <th colspan="7">July</th>
    </tr>
    <tr align="center">
        <td>S</td>
        <td>M</td>
        <td>T</td>
        <td>W</td>
        <td>T</td>
        <td>F</td>
        <td>S</td>
   </tr>
  <tr align="center">
      <td></td>
      <td>1×<br>受伤暂停训练</td>
      <td>2×<br>受伤暂停训练</td>
      <td>3×<br>受伤暂停训练</td>
      <td>4×<br>受伤暂停训练</td>
      <td>5×<br>受伤暂停训练</td>
      <td>6×<br>受伤暂停训练</td>
   </tr>
  <tr align="center">
      <td>7×<br>受伤暂停训练</td>
      <td>8×<br>受伤暂停训练</td>
      <td>9×<br>受伤暂停训练</td>
      <td>10×<br>受伤暂停训练</td>
      <td>11×<br>受伤暂停训练</td>
      <td>12×<br>受伤暂停训练</td>
      <td>13✔<br>田径场跑步 6 公里</td>
   </tr>
  <tr align="center">
      <td>14</td>
      <td>15</td>
      <td>16</td>
      <td>17</td>
      <td>18</td>
      <td>19</td>
      <td>20</td>
   </tr>
  <tr align="center">
      <td>21</td>
      <td>22</td>
      <td>23</td>
      <td>24</td>
      <td>25</td>
      <td>26</td>
      <td>27</td>
   </tr>
  <tr align="center">
      <td>28</td>
      <td>29</td>
      <td>30</td>
      <td>31</td>
      <td></td>
      <td></td>
      <td></td>
   </tr>
</table>

## LICENSE
<a rel="license" href="https://github.com/yanglbme/daily-exercise/blob/master/LICENSE"><img alt="Creative Commons License" style="border-width:0" src="./images/cc-by-sa-88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
